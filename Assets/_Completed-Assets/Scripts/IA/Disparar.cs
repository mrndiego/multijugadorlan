﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using Complete;

namespace BBUnity.Actions
{
    /// <summary>
    /// Dispara
    /// </summary>
    [Action("Acciones/Disparar")]
    [Help("Dispara el arma")]
    public class Disparar : GOAction
    {
        ///<value>Posicion del jugador</value>
        [InParam("vectorEnemigo")]
        [Help("Posicion del jugador")]
        public Vector3 vectorEnemigo;

        /// <summary>
        /// Ejecucion del disparo
        /// </summary>
        public override void OnStart()
        {
            TankShooting controlador = this.gameObject.GetComponent<TankShooting>();
            //Controlamos el disparo
            controlador.FireIA();
        }

        /// <summary>
        /// Devolucion del disparo
        /// </summary>
        /// <returns></returns>
        public override TaskStatus OnUpdate()
        {
          return TaskStatus.COMPLETED;
        }
    }
}
