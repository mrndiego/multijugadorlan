using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase encargada de mostrar los nombres por pantalla
/// </summary>
public class UIDirectionNickname : MonoBehaviour
{
	private Transform mainCameraTransform;

    // Start is called before the first frame update
    void Start()
    {
		mainCameraTransform = Camera.main.transform;
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.LookAt(transform.position + mainCameraTransform.rotation * Vector3.forward, mainCameraTransform.rotation * Vector3.up);
    }
}
