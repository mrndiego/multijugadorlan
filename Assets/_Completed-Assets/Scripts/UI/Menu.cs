using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Contiene las acciones para los botones y menus
/// </summary>
public class Menu : MonoBehaviour
{
    /// <summary>
    /// Evento que contiene la funcion para llamar a cambiarColor del jugador
    /// </summary>
    public static Action<int> cambiarColor;

    /// <summary>
    /// accion cuando se pulsa en iniciar
    /// </summary>
    public void iniciar()
    {
        SceneManager.LoadScene("Lobby");
    }

    /// <summary>
    /// Vuelve al menu de inicio
    /// </summary>
    public void menu()
    {
        SceneManager.LoadScene("Menu");
    }

    /// <summary>
    /// accion cuando se pulsa en salir
    /// </summary>
    public void salir()
    {
        Application.Quit(1);
    }

    /// <summary>
    /// accion cuando se pulsa en un color
    /// </summary>
    public void changeColor(int color)
    {
        Menu.cambiarColor(color);
    }

    /// <summary>
    /// accion cuando se pulsa en la flecha para ocultar y mostrar el panel de configuracion
    /// </summary>
    public void switchPanel()
    {
        ControlPaneles.switchOption();
    }
}
