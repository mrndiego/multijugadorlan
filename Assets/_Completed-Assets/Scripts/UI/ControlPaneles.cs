using Complete;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Gestor que se ocupa de reiniciar el nivel y gestionar los paneles
/// </summary>
public class ControlPaneles : MonoBehaviour
{
    /// <summary>
    /// Panel abieto
    /// </summary>
    public GameObject panelOption;

    /// <summary>
    /// Panel abieto
    /// </summary>
    public GameObject panelResumen;

    /// <summary>
    /// Flecha cerrado
    /// </summary>
    public GameObject flechaOption;

    /// <summary>
    /// Campo texto de victorias
    /// </summary>
    public TextMeshProUGUI victorias;

    /// <summary>
    /// Campo texto de derrotas
    /// </summary>
    public TextMeshProUGUI derrotas;

    /// <summary>
    /// Campos que almacena el numero de victorias y derrotas
    /// </summary>
    public int victoria = 0, derrota = 0;

    /// <summary>
    /// Funcion para solicitar abrir o cerrar el panel
    /// </summary>
    public static Action switchOption;

    /// <summary>
    /// Contiene el jugador local(tank)
    /// </summary>
    public static GameObject miJugador;

    /// <summary>
    /// Contiene todos los jugadores en local
    /// </summary>
    public static List<GameObject> jugadores = new List<GameObject>();

    /// <summary>
    /// Si entra en cuenta regresiva
    /// </summary>
    private bool cuentaRegresiva = false;

    /// <summary>
    /// Marcador de tiempo para que se reinicie
    /// </summary>
    public TextMeshProUGUI cuenta;

    /// <summary>
    /// Tiempo para que se reinicie
    /// </summary>
    private int cuentaNumero;

    /// <summary>
    /// Mensaje de quien ha ganado la ronda
    /// </summary>
    public TextMeshProUGUI mensajeVictoria;

    // Start is called before the first frame update
    void Start()
    {
        //Asignamos el evento
        switchOption = cambiarPanel;

        //Agregamos una funcion que se llame cada segundo para mostrar cuenta regresiva
        InvokeRepeating("restaRegresiva", 1,1);

        //Mostramos el panel resumen cuando iniciamos
        mostrarPanelResumen();
    }

    // Update is called once per frame
    void Update()
    {
        //Recogemos los jugadores en la partida
        GameObject[] jugadoresRestantes;
        jugadoresRestantes = GameObject.FindGameObjectsWithTag("Player");
       
        //Comprobamos numero de jugadores para finalizar la partida
        if (!cuentaRegresiva && jugadoresRestantes.Length == 1)
        {
            //Empezamos cuenta regresiva
            cuentaRegresiva = true;
            cuentaNumero = 14;
            Invoke("final",14);
            
        }
    }

    /// <summary>
    /// Se encarga de calcular y mostrar la cuenta regresiva en el UI
    /// </summary>
    private void restaRegresiva()
    {
        if (cuentaRegresiva)
        {
            if(cuentaNumero > 0)
            cuentaNumero--;

            cuenta.text = "Reinicio en: " + cuentaNumero+"s";
        }
        else
        {
            cuenta.text = "";
        }
    }

    /// <summary>
    /// Activa y programa la ocultacion del panel resumen
    /// </summary>
    private void mostrarPanelResumen()
    {
        //Preparamos panel para que se vea durante un periodo
        activarPanelResumen();
        Invoke("desactivarPanelResumen", 8);
    }

    /// <summary>
    /// Activa el panel resumen
    /// </summary>
    private void activarPanelResumen()
    {
        panelResumen.SetActive(true);
    }

    /// <summary>
    /// Desactiva el panel resumen
    /// </summary>
    private void desactivarPanelResumen()
    {
        panelResumen.SetActive(false);
    }

    /// <summary>
    /// Si esta activo desactiva el panel y viceversa
    /// </summary>
    private void cambiarPanel()
    {
        if (panelOption.activeSelf)
        {
            panelOption.SetActive(false);
            flechaOption.SetActive(true);
        }
        else
        {
            panelOption.SetActive(true);
            flechaOption.SetActive(false);
        }
    }

    /// <summary>
    /// metodo que se llama cuando termine la cuenta regresiva, prepara todo para reiniciar el nivel
    /// </summary>
    public void final()
    {
        GameObject[] jugadoresRestantes;
        jugadoresRestantes = GameObject.FindGameObjectsWithTag("Player");
       
        if (jugadoresRestantes.Length > 1)
        {
            //nos salimos porque se ha metido otro jugador
            cuentaRegresiva = false;
            return;
        }

        //Ejecutamos en servidor el reinicio
        if(EnemyManager.finalRonda != null)
        EnemyManager.finalRonda();

        //Si es servidor unicamente no tendra jugador
        if(miJugador != null)
        {
            if (miJugador.activeSelf)
            {
                //Ganamos
                masVictoria();
            }
            else
            {
                //Perdemos
                masDerrota();
            }

            //Actualizamos valores
            victorias.text = victoria + "";
            derrotas.text = derrota + "";

            //Indicamos quien ha ganado
            String nickname = "";
            foreach (GameObject jugador in jugadores)
            {
                if (jugador != null && jugador.activeSelf)
                {
                    //ha ganado
                    nickname = jugador.GetComponent<UINickname>().m_NicknameText;
                }
            }

            if (nickname.Equals("")){
                mensajeVictoria.text = "No hay ganador.";
            }
            else
            {
                mensajeVictoria.text = "El ganador es <b>" + nickname + "</b>";
            }

            //Mostramos panel
            mostrarPanelResumen();
        }

        //Activamos los tanks, le restauramos la vida y lo ponemos donde haya muerto
        foreach(GameObject jugador in jugadores)
        {
            if(jugador != null)
            {
                jugador.SetActive(true);
                TankHealth componenteVida = jugador.GetComponent<TankHealth>();
                componenteVida.m_CurrentHealth = componenteVida.m_StartingHealth;
                componenteVida.OnChangeHealth(componenteVida.m_CurrentHealth, componenteVida.m_StartingHealth);
            }
        }

        cuentaRegresiva = false;
    }

    /// <summary>
    /// Sumamos una derrota
    /// </summary>
    private void masDerrota()
    {
        derrota++;
    }

    /// <summary>
    /// Sumamos una victoria
    /// </summary>
    private void masVictoria()
    {
        victoria++;
    }
}
